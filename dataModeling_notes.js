/*
	
	What is a Data Model?
		► A data Model describes how data is organized & grouped in database.

		► By creating data models, we can anticipate which data will be manage by the DBMS in accordance to the application to be develop.



	Scenario:
		A course booking system application where a user can enroll into a course


	Type: COurse Booking System(Web App)

	Features:
		- USer Login (User Authentication)
		- User Registration

		Customer/Authenticated Users:
			- View Courses (All Active COurses)
			- Enroll Course

		Admin Users:
			- Add COurse
			- Update COurse
			- Archive/Unarachive (Soft delete/ reactivate the course - it's like hiding a file or non accessible)
			- View COurses (All Courses - either active/inactive)
			- View/Manage User Account

		All Users (guest, customers, admin)
			- View Active Courses



	
	User {
		id - unique identifier for the document // system generated
		username - String
		firstName - String
		lastName - String
		email - String
		password - String
		mobileNumber - String
		isAdmin: Boolean // no need to fillout by the customer, and it will be set to false
	}

	Course {
		id - unique identifier for the document // system generated
		name - String
		description - String
		price - Number
		slots - Number
		Instructor - String
		isActive - Boolean // True

	}
	
	User_Enrollment {
		id - id - unique identifier for the document // system generated
		userID - the unique identifier for the user (from the USer Document)
		courseID - the inique identifier for the course (from the Course Document)
		isPaid - Boolean
		dateEnrolled - dateTIme

	}

	Thee are two ways in creating a MOngoDB DAta Model:
		☼ Embedded Data Mdels
			- generally known as "DENORMALIZE" models and takes advantage of MongoDB's rich Documents

				- subdocument embedded in a parent document

			Example: 
				Parent Document {
					subDocument {
	
					}
				}

		☼ Reference Data Models
			- known as "NORMALIZE" data models and describes the relationship using reference between documents
			- Use doucument ID to connect a document to another document

	// Model Relationships

	Identifying Relationship Between Data Models

	☼ One-to-One
		- This relationship means that a model is exclusively related to only one model

			ex. A person can only have one employee ID on a given company.
			
		Sample DAta Model for One-to-One Relationship

			// Reference Data MOdel
			Employee: {
				"id": "2022webdev",
				"firstName": "Lex",
				"LastName": "Hufano",
				"email": "lhwebdev@mail.com"
			}

			Credentials: {
				"id": "creds_01",
				"emp_id": "2022webdev",
				"role": "Web Developer",
				"team": "tech"
			}

			// Embedded Data Model
			Employee: {
				"id": "2022webdev",
				"firstName": "Lex",
				"LastName": "Hufano",
				"email": "lhwebdev@mail.com"
				"credentials": {
								"id": "creds_01",
								"role": "Web Developer",
								"team": "tech"
							 }
			}


	☼ One-to-Many
		- One Model is related to multiple other models, However the other models are only related to one.

			ex. A person can have one or more email addresses.

			Person -many email address
			Many Email address - one person

			One Blog Post - many Comments
				- A blog post can have multiple commentsbut each comment should only refer to a single blog post
		
		Sample DAta Model for One-to-Many Relationship
		// Reference Data Model
		BLog {
			"id": "blog1-22",
			"title": "How to make Lumpia",
			"content": "This is an awesome way of making Lumpia",
			"createdOn": "09/21/22",
			"author": "blogwriter1"
		}

		Comments: {
			"id": "blog1comment1",
			"comment": "Very easy way",
			"author": "blogWriter2",
			"blog_id": "blog1-22"
		}

		{
			"id": "blog1comment2",
			"comment": "Ang Hirap",
			"author": "notSoHater2022",
			"blog_id": "blog1-22"
		}

		// Embedded Data Model
		BLog {
			"id": "blog1-22",
			"title": "How to make Lumpia",
			"content": "This is an awesome way of making Lumpia",
			"createdOn": "09/21/22",
			"author": "blogwriter1",
			"comments": [
							{
								"id": "blog1comment1",
								"comment": "Very easy way",
								"author": "blogWriter2",
							}

							{
								"id": "blog1comment2",
								"comment": "Ang Hirap",
								"author": "notSoHater2022",
							}
						]
		}

		


	☼ Many-to-Many
		- Multiple Documents are related to multiple documents

			ex. A book can be written by multiple authors and an author can write multiple books

			users - courses

			user can enroll many courses
			course can have many enrolless (user)

			Associative Entity


			//Sample Data Model

			
			//Reference Data Model
			User {
				"id": " " ,
				"username" : "" ,
				"firstName" : "", 
				"lastName" : "", 
				"email" : "", 
				"password" : "", 
				"mobileNumber" : 
				"isAdmin": " "
			}

			Course {
				"id" : " " ,
				"name" : " " ,
				"description" : " " , 
				"price" : " " ,
				"slots" : " " ,
				"Instructor": " " , 
				"isActive" : " " ,  

			}
			
			User_Enrollment {
				"id" : " ", id - unique identifier for the document // system generated
				"userID" : " ", the unique identifier for the user (from the USer Document)
				"courseID" : " ", the inique identifier for the course (from the Course Document)
				"isPaid" : " ",
				"dateEnrolled" : " " 

			}

			//Embedded Data Model
			Users {
				"id": " " ,
				"username" : "" ,
				"firstName" : "", 
				"lastName" : "", 
				"email" : "", 
				"password" : "", 
				"mobileNumber" : 
				"isAdmin": 
				"enrollments": `[
								{
									"id": " ",
									"courseId": " ",
									"isPaid":  " ",
									"dateEnrolled": " "
								}
							 ]`
			}
			
			Courses {
				"id": " ",
				"name": " ".
				"description": " ",
				"price": " ",
				"slots": " ",
				"Instructor": " ",
				"isActive": " ",
				"enrollees ": `[
								
									{
										id,
										userId,
										isPaid,
										dateEnrolled
									},

									{
										id,
										userId,
										isPaid,
										dateEnrolled
									}

								]`
			}



ForeignKey
	is a reference key to the primary key in the table
































*/



